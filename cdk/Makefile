LIBS_DIR := ../libs

VPATH := ..:$(LIBS_DIR)

LIBS_DEPS := $(shell $(MAKE) -s -r -C $(LIBS_DIR) print_deps)

OBJECTS := main.o cdk_main.o functions_ui.o
SOURCES := $(patsubst %.o,%.cpp,$(OBJECTS)) def.h

CXXFLAGS = \
	-Wall \
	-rdynamic \
	-D_GNU_SOURCE \
	-std=gnu++17 \
	-Wc++0x-compat

LDLIBS = \
	`pkg-config --cflags --cflags sigc++-2.0` \
	-I/usr/include/cdk \
	-I/usr/local/include/cdk \
	-I/usr/include/sigc++-2.0 \
	-I/usr/lib/$(DEB_HOST_MULTIARCH)/sigc++-2.0/include \
	-I$(LIBS_DIR)
	
LDFLAGS = \
	`pkg-config --libs sigc++-2.0` \
	-lnetaid \
	-lm \
	-lncurses \
	-lcdk \
	-lubus -lubox -lblobmsg_json -ljson_script -ljson-c \
	-lprocps -liw -lpstat

.SUFFIXES:

%.o: %.cpp
	g++ $(CXXFLAGS) -c $< -I.. $(LDLIBS)

simple-netaid-cdk: $(OBJECTS) $(LIBS_DIR)/libnetaidcdk.a
	g++ -o $@ $^ $(LDFLAGS)

$(OBJECTS): cdk_main.h functions_ui.h globals.h

$(LIBS_DIR)/libnetaidcdk.a:
	echo "---------------- $(LIBS_DEPS) -------------------------------"
	$(MAKE) -r -C $(LIBS_DIR) libnetaidcdk.a

.PHONY: clean cleanall print_objs print_srcs print_deps
.SILENT: print_objs print_srcs print_deps

clean:
	@rm -vf $(wildcard *.o) $(wildcard *~)

cleanall:
	@rm -vf $(wildcard *.o) $(wildcard *~) simple-netaid-cdk

#------- print objects, source-files and dependencies of the executable simple-netaid-cdk
print_objs:
	@echo $(OBJECTS)

print_srcs:
	@echo $(SOURCES)

print_deps:
	@echo $(SOURCES) globals.h libnetaidcdk.a

/*
 * cdk_main.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include "def.h"
#include "cdk_main.h"
#include "functions_ui.h"

#define setExitType(w,c)   setCdkExitType(ObjOf(w), &((w)->exitType), c)

#include <stdio.h>
#include <string.h>
#include <memory>
#include <signal.h>

#include <sys/un.h>           /*  UNIX socket  */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <assert.h>
#include <stdio.h>
#include <termios.h>     //termios, TCSANOW, ECHO, ICANON
#include <unistd.h>     //STDIN_FILENO

#define PROTOCOL 0
#define ACTIVE_WIFIS_SOCKET "/tmp/snetaid_active_wifis.socket"
#define MAX_SIZE 256
#define MAX_SUBMENU_ITEMS 5
#define TITLELINES 1

C_LINKAGE_BEGIN
#include <ubus/libubus.h>
#include <libubox/blobmsg_json.h>

#include <simple-netaid/sbuf.h>
#include <simple-netaid/ethlink.h>
#include <proc.h>
   
char* iproute();
void netproc(struct sbuf*);
int get_interface_status(const char*);
C_LINKAGE_END

namespace CdkSnetaid
{

static char **arr = NULL;
static int numWifis;

#define STDINBUFSIZE 8

#ifndef FD_SETSIZE
#define FD_SETSIZE  1024
#endif

// Usage: strncpy_t(str1, sizeof(str1), str2, strlen(str2));
// Copy string "in" with at most "insz" chars to buffer "out", which
// is "outsz" bytes long. The output is always 0-terminated. Unlike
// strncpy(), strncpy_t() does not zero fill remaining space in the
// output buffer:
static
char* strncpy_t(char* out, size_t outsz, const char* in, size_t insz)
{
   assert(outsz > 0);
   while(--outsz > 0 && insz > 0 && *in) { *out++ = *in++; insz--; }
   *out = 0;
   return out;
}

static
int util_split(const char *str, char c, char ***array)
{
    int count = 1;
    int token_len = 1;
    int i = 0;
    char *t;

    const char *p = str;
    while (*p != '\0') {
        if (*p == c)
            count++;
        p++;
    }

    *array = (char**) malloc(sizeof(char*) * count);
    if (*array == NULL) {
		return -ENOMEM;		
	}

    p = str;
    while (*p != '\0')
    {
        if (*p == c) {
            (*array)[i] = (char*) malloc( sizeof(char) * token_len );
            if ((*array)[i] == NULL) {
				return -ENOMEM;
			}

            token_len = 0;
            i++;
        }
        p++;
        token_len++;
    }
    
    (*array)[i] = (char*) malloc( sizeof(char) * token_len );
    if ((*array)[i] == NULL) {
		return -ENOMEM;
	}

    i = 0;
    p = str;
    t = ((*array)[i]);
    while (*p != '\0') {
        if (*p != c && *p != '\0') {
            *t = *p;
            t++;
        }
        else {
            *t = '\0';
            i++;
            t = ((*array)[i]);
        }
        p++;
    }

    return count;
}

/**
 * \brief Callback function for ubus_invoke to process result from the host
 * Here we just print out the message.
 */
static
void parse_output(struct ubus_request *req, int type, struct blob_attr *msg_t)
{
   int count = 0;
   char *tmp = NULL;   
   char *strmsg = NULL;  
   if(!msg_t) return;
   strmsg=blobmsg_format_json_indent(msg_t,true, 0); /* 0 type of format */   
   
   strmsg[strcspn(strmsg, "}")] = 0;
   count = util_split(strmsg , '|', &arr);
   tmp = rindex( arr[0], '"' );
   arr[0] = tmp + 1;
   arr[count-1][strcspn(arr[count-1], "\"")] = 0;
   free(strmsg);
   numWifis = count / 3;
}

static
int XXXCB (
   EObjectType cdktype GCC_UNUSED,
   void *object GCC_UNUSED,
   void *clientData GCC_UNUSED,
   chtype key GCC_UNUSED 
) {
   return (true);
}

static
CDKOBJS *bindableObject (EObjectType * cdktype, void *object)
{
   CDKOBJS *obj = (CDKOBJS *)object;

   if (obj != 0 && *cdktype == ObjTypeOf (obj))
   {
      if (*cdktype == vFSELECT)
      {
    *cdktype = vENTRY;
    object = ((CDKFSELECT *)object)->entryField;
      }
      else if (*cdktype == vALPHALIST)
      {
    *cdktype = vENTRY;
    object = ((CDKALPHALIST *)object)->entryField;
      }
   }
   else
   {
      object = 0;
   }
   return (CDKOBJS *)object;
}

CdkMain::CdkMain (string wired, string wireless) 
{
   x = y = 0;
   
   wired_device = wired;
   wireless_device = wireless;

   menuloc[0] = LEFT;
   menuloc[1] = LEFT;
   menuloc[2] = LEFT;
   menuloc[3] = LEFT; 
   
   //Connect Server signals to the signal handlers in Client.
   update_connector = new sigc::connection
    (
      f_ui->signal_ui_update().connect(sigc::mem_fun(*this, &CdkMain::on_callback_ui_update))
    );
      
   f_ui->signal_ui_display().connect(sigc::mem_fun(*this, &CdkMain::on_callback_ui_display));
   f_ui->signal_ui_delete().connect(sigc::mem_fun(*this, &CdkMain::on_callback_ui_delete));
}

CdkMain::~CdkMain()
{
   delete update_connector;
   destroyCDKMenu(menu);
   destroyCDKScreen(cdkscreen);
}

void CdkMain::get_status()
{   
   int a=1; /* disconnected */
   int b=get_interface_status(wired_device.c_str());
   int c=get_interface_status(wireless_device.c_str());
   
   const char *ifname = iproute();
   
   if(strcmp(ifname, "")) {
      if(!strcmp(ifname, wired_device.c_str())) a=2;
      else if(!strcmp(ifname, wireless_device.c_str())) a=3;
   }
   
   this->status = a*100 + b*10 +c;      
}

void CdkMain::cleanUpMenu()
{
   curs_set(0);

   /* Erase the sub-menu. */
   eraseCDKMenuSubwin (menu);
   wrefresh (menu->pullWin[menu->currentTitle]);
   
   /* Refresh the screen. */
   refreshCDKScreen (ScreenOf (menu));
   
   if(*iproute()==0)
      connected_ok = false;
   else
      connected_ok = true;
}

/*
 * The "%" operator is simpler but does not handle negative values.
 */
int CdkMain::wrapped (int within, int limit)
{
   if (within < 0)
      within = limit - 1;
   else if (within >= limit)
      within = 0;
   return within;
}

void CdkMain::drawItem (CDKMENU *menu, int item, int offset)
{
   writeChtype (menu->pullWin[menu->currentTitle],
      1, item + TITLELINES - offset,
      menu->sublist[menu->currentTitle][item],
      HORIZONTAL,
      0, menu->sublistLen[menu->currentTitle][item]);
}

/* Highlight the current sub-menu item. */
void CdkMain::selectItem (CDKMENU *menu, int item, int offset)
{
   writeChtypeAttrib (menu->pullWin[menu->currentTitle],
            1, item + TITLELINES - offset,
            menu->sublist[menu->currentTitle][item],
            menu->subtitleAttr,
            HORIZONTAL,
            0, menu->sublistLen[menu->currentTitle][item]);
}

void CdkMain::acrossSubmenus(CDKMENU *menu, int step)
{
   int next = wrapped (menu->currentTitle + step, menu->menuItems);

   if (next != menu->currentTitle)
   {
      /* Erase the menu sub-window. */
      eraseCDKMenuSubwin (menu);
      refreshCDKScreen (ScreenOf (menu));

      /* Set the values. */
      menu->currentTitle = next;
      menu->currentSubtitle = 0;
       
      /* Draw the new menu sub-window. */
      drawCDKMenuSubwin (menu);
      ObjOf (menu)->inputWindow = menu->titleWin[menu->currentTitle];
   }
}

void CdkMain::withinSubmenu(CDKMENU *menu, int step)
{
   int next = wrapped (menu->currentSubtitle + step, menu->subsize[menu->currentTitle]);

   if (next != menu->currentSubtitle)
   {
      CDKSCREEN *screen = ScreenOf (menu);
      int ymax = getmaxy (screen->window);

      if ((1 +
      getbegy (menu->pullWin[menu->currentTitle]) +
      menu->subsize[menu->currentTitle]) >= ymax)
      {
    menu->currentSubtitle = next;
    drawCDKMenuSubwin(menu);
      }
      else
      {
    /* Erase the old subtitle. */
    drawItem (menu, menu->currentSubtitle, 0);

    /* Set the values. */
    menu->currentSubtitle = next;

    /* Draw the new sub-title. */
    selectItem (menu, menu->currentSubtitle, 0);

    wrefresh (menu->pullWin[menu->currentTitle]);
      }

      ObjOf (menu)->inputWindow = menu->titleWin[menu->currentTitle];
   }
}

/*
 * Read from the input window, filtering keycodes as needed.
 */
int CdkMain::getcCDKObject (CDKOBJS *obj)
{
   EObjectType cdktype = ObjTypeOf (obj);
   CDKOBJS *test = bindableObject (&cdktype, obj);

   fd_set fdset;
   int fdin=0;   
   char stdinbuf[STDINBUFSIZE] = {0};
   
   FD_ZERO(&fdset);
   FD_SET(fdin, &fdset);
  
   struct termios oldt, newt;

   /*tcgetattr gets the parameters of the current terminal
    STDIN_FILENO will tell tcgetattr that it should write the settings
    of stdin to oldt*/
   tcgetattr( STDIN_FILENO, &oldt);
    /*now the settings will be copied*/
   newt = oldt;

    /*ICANON normally takes care that one line at a time will be processed
    that means it will return if it sees a "\n" or an EOF or an EOL*/
   newt.c_lflag &= ~(ICANON);          

   /*Those new settings will be set to STDIN
   TCSANOW tells tcsetattr to change attributes immediately. */
   fdin = STDIN_FILENO;
   tcsetattr( fdin, TCSANOW, &newt);
   
   keystroke = KEY_ESC;
   menu->exitType = vESCAPE_HIT;
  
   if(test != 0 && select(fdin + 1, &fdset, NULL, NULL, NULL) > 0) {
      if(FD_ISSET(fdin, &fdset)) {
         if((read(fdin, stdinbuf, STDINBUFSIZE)) > 0) {
         int keystroke = (int)stdinbuf[0]*100 + (int)stdinbuf[2];

            switch(keystroke) {
               case 2700:
                  keystroke = KEY_ESC;
                  menu->exitType = vESCAPE_HIT;
                  break;
               case 1300:
                  keystroke = KEY_ENTER;
                  menu->exitType = vNORMAL;
                  break;
               case 900:
                  keystroke = KEY_TAB;
                  acrossSubmenus(menu, 1);
                  menu->exitType = vEARLY_EXIT;
                  break;
               case 2765:
                  withinSubmenu (menu, -1);
                  keystroke = KEY_UP;
                  menu->exitType = vEARLY_EXIT;
                  break;
               case 2766:
                  withinSubmenu (menu, 1);
                  keystroke = KEY_DOWN;
                  menu->exitType = vEARLY_EXIT;
                  break;
               case 2767:
                  acrossSubmenus(menu, 1);
                  keystroke = KEY_RIGHT;
                  menu->exitType = vEARLY_EXIT;
                  break;
               case 2768:
                  acrossSubmenus(menu, -1);
                  keystroke = KEY_LEFT;
                  menu->exitType = vEARLY_EXIT;
                  break;
               default:
                  break;
            }
         }
      }
   }
   
   /*restore the old settings*/
   tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    
   return keystroke; 
}

/*
 * Use this function rather than getcCDKObject(), since we can extend it to
 * handle wide-characters.
 */
int CdkMain::getchCDKObject (CDKOBJS *obj, boolean *functionKey)
{
   int key = CdkMain::getcCDKObject (obj);
   *functionKey = (key >= KEY_MIN && key <= KEY_MAX);
   return key;
}

/*
 * This activates the CDK Menu.
 */
int CdkMain::activateCDKMenu (CDKMENU *menu)
{
   chtype input __attribute__((unused));
   boolean functionKey;

   /* Draw in the screen. */
   refreshCDKScreen (ScreenOf (menu));

   /* Display the menu titles. */
   drawCDKMenu (menu, ObjOf (menu)->box);

   /* Highlight the current title and window. */
   drawCDKMenuSubwin (menu);
   ObjOf (menu)->inputWindow = menu->titleWin[menu->currentTitle];
   input = (chtype)CdkMain::getchCDKObject (ObjOf (menu), &functionKey);
 
   return -1;
}

void CdkMain::display()
{   
   curs_set(1);
    
   cdkscreen = 0;
   cdkscreen = initCDKScreen (0);

   initCDKColor();
     
   while(1) {
      const char *menulist[MENU_ITEMS][MAX_SUB_ITEMS];
      vector<string> submenu_0, submenu_1, submenu_2, submenu_3;

      menu = 0;
      get_status();
      
      submenu_0.begin();
      submenu_1.begin();
      submenu_2.begin();
      submenu_3.begin();
      
      submenu_0.push_back("</B>Menu<!B>");      
      submenu_0.push_back("</B>Show details...<!B>");
      //if(m_tty < 0)
         submenu_0.push_back("</B/5>Exit<!B!5>");
      
      submenu_1.push_back("</B> Wired <!B>");
      submenu_1.push_back("</B>Show " + wired_device + "<!B>");
      
      submenu_2.push_back("</B> Wireless <!B>");
      if(wireless_device.size()) submenu_2.push_back("</B>Show " + wireless_device + "<!B>");
      
      submenu_3.push_back("</B>Help<!B>");
      submenu_3.push_back("</B>About...<!B>");
      
      switch(this->status) {
         case 100:
            submenu_1.push_back("</B/24>Bring up " + wired_device + "<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Bring up " + wireless_device + "<!B!24>");
            break;
            
         case 101:
            submenu_1.push_back("</B/24>Bring up " + wired_device + "<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Scan active wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Saved wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/16>Bring down " + wireless_device + "<!B!16>");
            break;
            
         case 110:
            submenu_1.push_back("</B/24>Connect to " + wired_device + "<!B!24>");
            submenu_1.push_back("</B/16>Bring down " + wired_device + "<!B!16>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Bring up " + wireless_device + "<!B!24>");
            break;
            
         case 111:
            submenu_1.push_back("</B/24>Connect to " + wired_device + "<!B!24>");
            submenu_1.push_back("</B/16>Bring down " + wired_device + "<!B!16>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Scan active wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Saved wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/16>Bring down " + wireless_device + "<!B!16>");
            break;
            
         case 210:
            submenu_1.push_back("</B/16>Disconnect from " + wired_device + "<!B!16>");
            submenu_1.push_back("</B/16>Bring down " + wired_device + "<!B!16>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Bring up " + wireless_device + "<!B!24>");
            break;
            
         case 211:
            submenu_1.push_back("</B/16>Disconnect " + wired_device + "<!B!16>");
            submenu_1.push_back("</B/16>Bring down " + wired_device + "<!B!16>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Scan active wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Saved wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/16>Bring down " + wireless_device + "<!B!16>");
            break;
            
         case 301:
            submenu_1.push_back("</B/24>Bring up " + wired_device + "<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Scan active wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Saved wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/16>Disconnect from " + wireless_device + "<!B!16>");
            if(wireless_device.size()) submenu_2.push_back("</B/16>Bring down " + wireless_device + "<!B!16>");
            break;
            
         case 311:
            submenu_1.push_back("</B/16>Bring down " + wired_device + "<!B!16>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Scan active wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/24>Saved wifis<!B!24>");
            if(wireless_device.size()) submenu_2.push_back("</B/16>Disconnect from " + wireless_device + "<!B!16>");
            if(wireless_device.size()) submenu_2.push_back("</B/16>Bring down " + wireless_device + "<!B!16>");
            break;
            
         default: break;
      }
      
      submenusize[0] = submenu_0.size();
      submenusize[1] = submenu_1.size();
      submenusize[2] = submenu_2.size();
      submenusize[3] = submenu_3.size();
      
      int j=0;
      vector<string>::iterator iter;
      for (iter=submenu_0.begin(); iter!=submenu_0.end(); iter++) {
         menulist[0][j] = (const char*)(*iter).c_str();
         j++;
      }
      
      j=0;
      for (iter=submenu_1.begin(); iter!=submenu_1.end(); iter++) {
         menulist[1][j] = (const char*)(*iter).c_str();
         j++;
      }
      
      j=0;
      for (iter=submenu_2.begin(); iter!=submenu_2.end(); iter++) {
         menulist[2][j] = (const char*)(*iter).c_str();
         j++;
      }
      
      j=0;
      for (iter=submenu_3.begin(); iter!=submenu_3.end(); iter++) {
         menulist[3][j] = (const char*)(*iter).c_str();
         j++;
      }  
         
      /* Create the menu. */
      menu = newCDKMenu (
         cdkscreen,
         menulist,
         MENU_ITEMS,
         submenusize,
         menuloc,
         TOP,
         A_UNDERLINE,
         A_REVERSE
      );      
     
     setCDKMenuCurrentItem(menu, x, y);
      
      /* Draw the CDK screen. */
      refreshCDKScreen (cdkscreen);
      
      drawCDKMenu(menu, false);
      drawCDKMenuSubwin(menu);

      /* Activate the menu. */
      int selection __attribute__((unused)) = CdkSnetaid::CdkMain::activateCDKMenu (menu);
      
      /* Determine how the user exited from the widget. */
      if (menu->exitType == vESCAPE_HIT) {

         fd_set fds;
         time_t t = 0;       
         struct timeval tv;

Select:      
         FD_ZERO(&fds);    
          
         tv.tv_sec = 1;
         tv.tv_usec = 0;
      
         if (t) {
            int delay = t - time(NULL);
            if (delay < 0)
               tv.tv_sec = 0;
            else if (delay < tv.tv_sec)
               tv.tv_sec = delay;
         } 

         if (select(FD_SETSIZE, &fds, NULL, NULL, &tv) < 0) {
            if (errno == EINTR) 
               goto Select;
         }
         
         getCDKMenuCurrentItem(menu, &x, &y);
         cleanUpMenu();
         continue;
      }
      
      else if(menu->exitType == vEARLY_EXIT) {
         getCDKMenuCurrentItem(menu, &x, &y);
         cleanUpMenu();
         continue;
      }

      else if (menu->exitType == vNORMAL) {
        
         switch ( menu->currentTitle * 10 + menu->currentSubtitle ) {
            
            case 0:
               cleanUpMenu();
               show_details();
               break;

            case 1: /* Quit */
               cleanUpMenu();
               kill_child(SIGTERM);
               p = QUIT;
               return;
         
            case 10:
               cleanUpMenu();
               show_device(wired_device);
               break;
               
            case 11: 
                    switch(this->status) {
                     case 100:
                     case 101:
                     case 301:
                        cleanUpMenu();
                        y = 0;
                        p = WIRED_UP;
                        return;
                        
                     case 110:
                     case 111:
                        {
                           int j=0, rc;
                           while(j<3) {
							  rc = ethlink(wired_device.c_str()); 
							  if(rc == 1)
							     break;
							  j++;
							  sleep(1);
                           }     
                           if(rc == 1) {
                              cleanUpMenu();
                              y = 0;
                              p = WIRED_CONNECTION;
                              return;
                           } else {
                              const char *msg[3];
                              msg[0] = "";
                              msg[1] = "<C></B>                The ethernet cable is unplugged.                 <!B>";
                              msg[2] = "";
                              popupLabel (cdkscreen, (CDK_CSTRING2) msg, 3);
                              break;   // Cancel
                           }
                        }      
                                             
                     case 210:
                     case 211:
                        cleanUpMenu();
                        y = 0;
                        p = WIRED_DISCONNECT;
                        return;
                        
                     case 311:
                        cleanUpMenu();
                        y = 0;
                        p = WIRED_DOWN;
                        return;
                        
                     default: break;
                  }
                  
                  break;
                  
            case 12:
                  cleanUpMenu();
                  y = 0;
                  p = WIRED_DOWN;
                  return;
         
            case 20:
                  cleanUpMenu();
                  show_device(wireless_device);
                  break;
               
            case 21:
                  switch(this->status) {
                     case 100:
                     case 110:
                     case 210:
                        cleanUpMenu();
                        y = 0;
                        p = WIRELESS_UP;
                        return;
                        
                     case 101:
                     case 111:
                     case 211:
                     case 301:
                     case 311:
                        if(wireless_device.size()) {
                           cleanUpMenu();
                           p = scrolled_list();
                           p = p + WIRELESS_CONNECTION;
                           if(*iproute()==0) {
                              if(p == WIRELESS_CONNECTION || p == INSTALL_AND_CONNECT) {
                                 passwd_entry();
                                 if(p == INSTALL_AND_CONNECT)
                                    filename_entry();
                              }
                              else {
                                 break;   /* Cancel */
                              }
                           }
                           else {
                       if(p == WIRELESS_CONNECTION || p == INSTALL_AND_CONNECT) {
                         const char *msg[3];
                         msg[0] = "";
                         msg[1] = "<C></B>                You are already conected!                <!B>";
                         msg[2] = "";
                         popupLabel (cdkscreen, (CDK_CSTRING2) msg, 3);
                                 break;   /* Cancel */
                       }
                              else {
                                 break;   /* Cancel */
                              }
                     }
                  }
                  cleanUpMenu();
                  y = 0;
                  return;
                        
                     default: 
                        break;
                  }
                  
                  break;
                  
            case 22:
                  switch(this->status) {
                     case 101:
                     case 111:
                     case 211:
                     case 301:
                     case 311:
                        {
                           cleanUpMenu();
                           int button_action = show_saved_wifis();
                           if(button_action == 2) break;  /* cancel */
                           p = CONNECT_TO_SAVED + button_action;
                           cleanUpMenu();
                           y = 0;
                           return;
                   }
                     
                     default:
                        break;
                  }
               
                  break;
               
            case 23:
                  switch(this->status) {
                     case 101:
                     case 111:
                     case 211:
                        cleanUpMenu();
                        y = 0;
                        p = WIRELESS_DOWN;
                        return;
                     
                     case 301:
                     case 311:
                        cleanUpMenu();
                        y = 0;
                        p = WIRELESS_DISCONNECT;
                        return;
                     
                     default:
                        break;
                  }
               
                  break;
               
            case 24:
                  switch(this->status) {
                     case 301:
                     case 311:
                        cleanUpMenu();
                        y = 0;
                        p = WIRELESS_DOWN;
                        return;
                     
                     default:
                        break;
                  }               
                  break;
                  
            case 30:
                  show_info();
                  break;      
               
            default:
                  break;
            
         }   // switch
        
         /* Clean up. */
         cleanUpMenu ();
         
      }  // else if
      
   } // while
}

void CdkMain::show_details()
{
   struct sbuf info;
   const char *msg[9];
   
   sbuf_init(&info);
   netproc(&info);
   
   /* get the first token */
   char ch[2]="\n";
   char *token = strtok(info.buf, ch);
   
   /* walk through other tokens, taking only the essids */
   int count=0;
   msg[count] = 0;
   while( token != NULL ) {
      msg[count] = token;
      count++; msg[count] = 0;
      token = strtok(NULL, ch);
   }
   
   popupLabel(cdkscreen, (CDK_CSTRING2) msg, count);
   
   /* Clean up */
   free(info.buf);
}

void CdkMain::show_info()
{
   const char *msg[14];
   msg[0] = "";
   msg[1] = "   Simple-netaid is free software: you can redistribute   ";
   msg[2] = "   it and/or modify it under the terms of the GNU General ";
   msg[3] = "   Public License as published by the Free Software  ";
   msg[4] = "   Foundation, either version 3 of the License, or (at your ";
   msg[5] = "   option) any later version.";
   msg[6] = "";
   msg[7] = "   Simple-netaid is distributed in the hope that it will be   ";
   msg[8] = "   useful, but WITHOUT ANY WARRANTY, without even the implied  ";
   msg[9] = "   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR  ";
   msg[10] = "   PURPOSE.";
   msg[11] = "";
   msg[12] = "   See the GNU General Public License for more details. ";
   msg[13] = "";
   
   popupLabel(cdkscreen, (CDK_CSTRING2) msg, 14);
}

void CdkMain::show_device(string device)
{
   string label;
   const char *msg[1];
   
   if( get_interface_status(device.c_str()) )
      label = "<C>   " + device + "   |  UP   ";
   else label = "<C>   " + device + "   |  DOWN   ";
   
   msg[0] = (const char*)label.c_str();
   popupLabel (cdkscreen, (CDK_CSTRING2) msg, 1);
}

int CdkMain::scrolled_list()
{
   int rc __attribute__((unused)) = 0;
   CDKSCROLL *scrollList = 0;
   CDKLABEL *CDKlabel = 0;
   const char *title = "<C></5>Choose from the networks below<!5>:\n";
   const char *msg[3];
   char **wifis = 0;
   int selection;

   CDK_PARAMS params;
   CDKparseParams (0, nullptr, &params, "acs:t:" CDK_CLI_PARAMS);
   
   curs_set(0);
   msg[0] = "";
   msg[1] = "<C></B>                Scanning available active wifis... Please, wait.                <!B>";
   msg[2] = "";
   CDKlabel = newCDKLabel (cdkscreen, CENTER, CENTER, (CDK_CSTRING2) msg, 3, FALSE, FALSE);
   drawCDKLabel (CDKlabel, TRUE);
	
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id); 	/* ID */
   blobmsg_add_string(&bb_t,"ifname", wireless_device.c_str());   /* IFNAME */
   rc = ubus_invoke(ctx, host_id, "scan_active_wifis", bb_t.head, parse_output, 0, 10000);

   destroyCDKLabel (CDKlabel);
   curs_set(1);
 
   // Memory allocation:
   wifis = (char **)malloc(numWifis * sizeof(char *));
   for (int i=0; i<numWifis; i++) 
      wifis[i] = (char*)malloc(MAX_SIZE * sizeof(char));   
   
   int i=0, j=0, k=0;
   
   // walk through items, taking only the essid into account
   while( i < numWifis ) {
      if(j==1) {
         if (strstr(arr[k], "x00\x00")) {
            strcpy(wifis[i], arr[k]);
         } else {
            strcpy(wifis[i], arr[k]);
         }
         i++;         
      }   
      j++; k++;
      if(j==3) j=0;
   }
   
   // Create the scrolling list.
   scrollList = newCDKScroll (
      cdkscreen,
      CDKparamValue (&params, 'X', CENTER),
      CDKparamValue (&params, 'Y', CENTER),
      CDKparsePosition (
         CDKparamString2 (&params, 's', "RIGHT")
      ),
      CDKparamValue (&params, 'H', 10),
      CDKparamValue (&params, 'W', 50),
      CDKparamString2 (&params, 't', title),
      (CDK_CSTRING2)wifis,
      numWifis,
      FALSE,
      A_REVERSE,
      CDKparamValue (&params, 'N', TRUE),
      CDKparamValue (&params, 'S', FALSE)
   );
   
   // Is the scrolling list null?
   if (scrollList == 0) {
      
      destroyCDKScreen (cdkscreen);
      endCDK ();
      
      printf ("Cannot make scrolling list. Is the window too small?\n");
      ExitProgram (EXIT_FAILURE);
   }
   
   if (CDKparamNumber (&params, 'c')) {
      
      setCDKScrollItems (scrollList, (CDK_CSTRING2)wifis, numWifis, TRUE);
   }
   
   if (CDKparamNumber (&params, 'a')) {
      
      drawCDKScroll (scrollList, 1);
      
      setCDKScrollPosition (scrollList, 10);
      drawCDKScroll (scrollList, 1);
      napms (3000);
      
      setCDKScrollPosition (scrollList, 20);
      drawCDKScroll (scrollList, 1);
      napms (3000);
      
      setCDKScrollPosition (scrollList, 30);
      drawCDKScroll (scrollList, 1);
      napms (3000);
      
      setCDKScrollPosition (scrollList, 70);
      drawCDKScroll (scrollList, 1);
      napms (3000);
   }
   
   // Activate the scrolling list.
   selection = activateCDKScroll (scrollList, 0);
   
   // Determine how the widget was exited.
   if (scrollList->exitType == vESCAPE_HIT) {
      
      const char *msg[3];
      msg[0] = "<C>You hit escape. No file selected.";
      msg[1] = "";
      msg[2] = "<C>Press any key to continue.";
      popupLabel (cdkscreen, (CDK_CSTRING2)msg, 3);
      
   } else if (scrollList->exitType == vNORMAL) {
      
      const char *msg[3];
      memset(&m_essid, 0, 256);
      strncpy_t(m_essid, 256, chtype2Char (scrollList->item[selection]), strlen(chtype2Char (scrollList->item[selection])));
      msg[0] = "<C>This network requires encryption to be enabled.";
      msg[1] = strdup(m_essid);
      msg[2] = "";
      
      CDKDIALOG   *question;
      const char   *buttons[40];
      CDK_PARAMS   prms;
      
      CDKparseParams (0, nullptr, &prms, CDK_MIN_PARAMS);
      
      buttons[0] = "  Connect  ";
      buttons[1] = "  Connect + Save  ";
      buttons[2] = "  Cancel  ";
      
      destroyCDKScroll(scrollList);
      
      /* Create the dialog box. */
      question = newCDKDialog (
         cdkscreen,
         CDKparamValue (&params, 'X', CENTER),
         CDKparamValue (&params, 'Y', CENTER),
         (CDK_CSTRING2) msg, 3,
         (CDK_CSTRING2) buttons, 3,
         A_REVERSE, TRUE,
         CDKparamValue (&params, 'N', TRUE),
         CDKparamValue (&params, 'S', FALSE)
      );
      
      /* Check if we got a null value back. */
      if (question == (CDKDIALOG *)0) {
         destroyCDKScreen (cdkscreen);
         
         /* End curses... */
         endCDK ();
         
         printf ("Cannot create the dialog box. ");
          printf ("Is the window too small?\n");
          ExitProgram (EXIT_FAILURE);
      }        
      
      /* Activate the dialog box. */
      selection = activateCDKDialog (question, (chtype *)0);
      destroyCDKDialog (question);
      
   }
   
   // Clean up.
   for(int i=0;i<numWifis;i++) free(*(wifis+i));
   free(wifis);
   
   return selection;
}

void CdkMain::passwd_entry()
{
   CDKENTRY *directory  = 0;
   const char *title = "<C>Enter the private key (leave in blank for open wifis).";
   const char *label = " </U/5>Password<!U!5>: ";
   const char *msg[10];
   
   CDK_PARAMS params;
   CDKparseParams (0, nullptr, &params, CDK_MIN_PARAMS);
   
   // Create the entry field widget.
   directory = newCDKEntry (
      cdkscreen,
      CDKparamValue (&params, 'X', CENTER),
      CDKparamValue (&params, 'Y', CENTER),
      title, label, A_NORMAL, '.', vMIXED,
      40, 0, MAX_SIZE,
      CDKparamValue (&params, 'N', TRUE),
      CDKparamValue (&params, 'S', FALSE)
   );
   
   bindCDKObject (vENTRY, directory, '?', XXXCB, 0);
   
   // Is the widget null?
   if (directory == 0) {
      // Clean up.
      destroyCDKScreen (cdkscreen);
      endCDK ();
      
      printf ("Cannot create the entry box. Is the window too small?\n");
      ExitProgram (EXIT_FAILURE);
   }
   
   // Draw the screen.
   refreshCDKScreen (cdkscreen);
   setCDKEntry (directory, nullptr, 0, MAX_SIZE, TRUE);
   
   // Activate the entry field.
   const char *info = activateCDKEntry (directory, 0);
   
   // Tell them what they typed.
   if (directory->exitType == vESCAPE_HIT)
   {
      msg[0] = "<C>You hit escape. No information passed back.";
      msg[1] = "",
      msg[2] = "<C>Press any key to continue.";
      
      destroyCDKEntry (directory);
      popupLabel (cdkscreen, (CDK_CSTRING2) msg, 3);
      
   } else if (directory->exitType == vNORMAL) {
      strncpy_t(m_passwd, 256, info, strlen(info));
   }
   
   destroyCDKEntry (directory);
}

void CdkMain::filename_entry()
{
   CDKENTRY *directory  = 0;
   const char *title = "<C>Choose a descriptive name for the installed wifi.";
   const char *label = " </U/5>Filename<!U!5>: ";
   const char *msg[10];
   
   CDK_PARAMS params;
   CDKparseParams (0, nullptr, &params, CDK_MIN_PARAMS);
   
   // Create the entry field widget.
   directory = newCDKEntry (
      cdkscreen,
      CDKparamValue (&params, 'X', CENTER),
      CDKparamValue (&params, 'Y', CENTER),
      title, label, A_NORMAL, '.', vMIXED,
      40, 0, MAX_SIZE,
      CDKparamValue (&params, 'N', TRUE),
      CDKparamValue (&params, 'S', FALSE)
   );
   
   bindCDKObject (vENTRY, directory, '?', XXXCB, 0);
   
   // Is the widget null?
   if (directory == 0) {
      // Clean up.
      destroyCDKScreen (cdkscreen);
      endCDK ();
      
      printf ("Cannot create the entry box. Is the window too small?\n");
      ExitProgram (EXIT_FAILURE);
   }
   
   // Draw the screen.
   refreshCDKScreen (cdkscreen);
   setCDKEntry (directory, nullptr, 0, MAX_SIZE, TRUE);
   
   // Activate the entry field.
   const char *info = activateCDKEntry (directory, 0);
   
   // Tell them what they typed.
   if (directory->exitType == vESCAPE_HIT) {
      msg[0] = "<C>You hit escape. No information passed back.";
      msg[1] = "",
      msg[2] = "<C>Press any key to continue.";
      
      destroyCDKEntry (directory);
      popupLabel (cdkscreen, (CDK_CSTRING2) msg, 3);
      
   } else if (directory->exitType == vNORMAL) {
      memset(&m_filename, 0, 256);
      strncpy_t(m_filename, 256, info, strlen(info));
   }
   
   destroyCDKEntry (directory);
}

int CdkMain::show_saved_wifis()
{
   DIR *dir;
   struct dirent *ent;
   
   vector<string> wifis;
   CDKSCROLL *scrollList1 = 0;
   const char *title = "<C></5>Choose from the networks below<!5>:\n";
   char **saved_wifis = 0;
   int selection;
   
   CDK_PARAMS params;
   CDKparseParams (0, nullptr, &params, "acs:t:" CDK_CLI_PARAMS);
   
   dir = opendir ("/etc/network/wifi");
   if (!dir) {
      const char *msg[3];
      msg[0] = "";
      msg[1] = "<C></B>                There is no wifi saved!                <!B>",
      msg[2] = "";
      popupLabel (cdkscreen, (CDK_CSTRING2) msg, 3);
      selection=10;
      goto end;
   }
   
   wifis.begin();
   while( (ent = readdir (dir)) ) {
      if ( (strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0) )
         wifis.push_back(ent->d_name);
   }
   
   closedir (dir);
   
   if(wifis.size()==0) {
      const char *msg[3];
      msg[0] = "";
      msg[1] = "<C></B>                There is no wifi saved!                <!B>",
      msg[2] = "";
      popupLabel (cdkscreen, (CDK_CSTRING2) msg, 3);
      selection=10;
      goto end;
   }
   
   // Memory allocation:
   saved_wifis = (char **)malloc(wifis.size() * sizeof(char*));
   for (unsigned i=0; i<wifis.size(); i++)
      saved_wifis[i] = (char*)malloc(MAX_SIZE * sizeof(char));

   for(unsigned i=0;i<wifis.size();i++)
      strcpy(saved_wifis[i], wifis[i].c_str());
      
   // Create the scrolling list.
   scrollList1 = newCDKScroll (
      cdkscreen,
      CDKparamValue (&params, 'X', CENTER),
      CDKparamValue (&params, 'Y', CENTER),
      CDKparsePosition (
         CDKparamString2 (&params, 's', "RIGHT")
      ),
      CDKparamValue (&params, 'H', 10),
      CDKparamValue (&params, 'W', 50),
      CDKparamString2 (&params, 't', title),
      (CDK_CSTRING2)saved_wifis,
      wifis.size(),
      FALSE,
      A_REVERSE,
      CDKparamValue (&params, 'N', TRUE),
      CDKparamValue (&params, 'S', FALSE)
   );
   
   // Is the scrolling list null?
   if (scrollList1 == 0) {
      destroyCDKScreen (cdkscreen);
      endCDK ();
      
      printf ("Cannot make scrolling list. Is the window too small?\n");
      ExitProgram (EXIT_FAILURE);
   }
   
   if (CDKparamNumber (&params, 'c')) {
      setCDKScrollItems (scrollList1, (CDK_CSTRING2)saved_wifis, wifis.size()+1, TRUE);
   }
   
   if (CDKparamNumber (&params, 'a')) {
      drawCDKScroll (scrollList1, 1);
      
      setCDKScrollPosition (scrollList1, 10);
      drawCDKScroll (scrollList1, 1);
      napms (3000);
      
      setCDKScrollPosition (scrollList1, 20);
      drawCDKScroll (scrollList1, 1);
      napms (3000);
      
      setCDKScrollPosition (scrollList1, 30);
      drawCDKScroll (scrollList1, 1);
      napms (3000);
      
      setCDKScrollPosition (scrollList1, 70);
      drawCDKScroll (scrollList1, 1);
      napms (3000);
   }
   
   // Activate the scrolling list.
   selection = activateCDKScroll (scrollList1, 0);
   
   // Determine how the widget was exited.
   if (scrollList1->exitType == vESCAPE_HIT) {
      
      const char *msg[3];
      msg[0] = "<C>You hit escape. No file selected.";
      msg[1] = "";
      msg[2] = "<C>Press any key to continue.";
      popupLabel (cdkscreen, (CDK_CSTRING2)msg, 3);
      
   } else if (scrollList1->exitType == vNORMAL) {
      
      filename = string(chtype2Char(scrollList1->item[selection]));
      filename = "/etc/network/wifi/" + filename;
      memset (&m_filename, 0, 256);
      strncpy_t (m_filename, 256, filename.c_str(), filename.length() +1);
      
      
      char **info = 0;
      int lines = CDKreadFile (filename.c_str(), &info);
      if (lines == -1) { 
         printf ("Could not open \"%s\"\n", filename.c_str());
         ExitProgram (EXIT_FAILURE);
      }
      
      CDKVIEWER *example   = 0;
      char vTitle[256];
      const char *buttons[40];
      
      buttons[0] = "  Connect  ";
      buttons[1] = "  Remove  ";
      buttons[2] = "  Cancel  ";
      
      /* Create the file viewer to view the file selected. */
      example = newCDKViewer (
         cdkscreen,
         CENTER,
         CENTER, 12, 80,
         (CDK_CSTRING2)buttons, 3,
         A_REVERSE, FALSE, FALSE
      );
      
      /* Set up the viewer title, and the contents to the widget. */
      sprintf (vTitle, "<C></B>%20s<!B>", filename.c_str());
      setCDKViewer (
         example,
         vTitle,
         (CDK_CSTRING2)info,
         lines,
         A_REVERSE, TRUE, FALSE, TRUE
      );
      
      CDKfreeStrings (info);
      
      /* Activate the viewer widget. */
      selection = activateCDKViewer (example, 0);
      
      /* Check how the person exited from the widget. */
      if (example->exitType == vESCAPE_HIT) {
         
         const char *mesg[4];
         mesg[0] = "<C>Escape hit. No Button selected.";
         mesg[1] = "";
         mesg[2] = "<C>Press any key to continue.";
         popupLabel (cdkscreen, (CDK_CSTRING2)mesg, 3);
         
      } else if ( example->exitType == vNORMAL && selection==0 && *iproute()!=0 ) {
         const char *msg[3];
         msg[0] = "";
         msg[1] = "<C></B>                You are already conected!                <!B>";
         msg[2] = "";
         popupLabel (cdkscreen, (CDK_CSTRING2) msg, 3);
         selection=2;
      }
      
      destroyCDKViewer(example);
   }

end:
   // Clean up.
   for(unsigned i=0;i<wifis.size();i++) free(*(saved_wifis+i));
   free(saved_wifis);
   
   destroyCDKScroll(scrollList1);
   
   return selection;
}

void CdkMain::on_callback_ui_display()
{   
   this->display();
}

/*
  Called from sigact.c (SIGTERM, SIGINT, SIGQUIT)
  We cannot use C++ fuctions within the signal handler
  Instead, we make use of a C++ wrapper to cleanup the 
  the memory allocated by ncurses interface.

  Further info:
  - https://wiki.sei.cmu.edu/confluence/display/cplusplus/MSC54-CPP.+A+signal+handler+must+be+a+plain+old+function
*/
void CdkMain::on_callback_ui_update()
{
   update_connector->block();
   int prev = this->status;
   get_status();
   if(this->status == prev)
      return;
      
   cleanUpMenu();
   setCDKMenuCurrentItem(menu, x, 0);
   //usleep(2500);
   update_connector->unblock();
   
   //setCDKMenuCurrentItem(menu, 0, 0);
   //rc = injectCDKMenu(menu, KEY_ENTER);
}

void CdkMain::on_callback_ui_delete()
{
   int rc __attribute__((unused)) = 0;
   setCDKMenuCurrentItem(menu, 0, 1);
   rc = injectCDKMenu(menu, KEY_ENTER);
}

} // namespace CdkSnetaid

 /*
  * def.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#ifndef __DEF_H__
#define __DEF_H__

/*-------------------------- Global constant --------------------------------*/

extern char 
   m_essid[256],
   m_passwd[256],
   m_filename[256];

#define QUIT                   1   
#define WIRED_UP               2
#define WIRED_DOWN             3
#define WIRED_DISCONNECT       4
#define WIRED_CONNECTION       5
#define WIRELESS_UP            6
#define WIRELESS_DOWN          7
#define WIRELESS_CONNECTION    8
#define INSTALL_AND_CONNECT    9
#define WIRELESS_DISCONNECT   10
#define CONNECT_TO_SAVED      11
#define UNINSTALL             12
#define DEFAULT               13 

#endif // __DEF_H__


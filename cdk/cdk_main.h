/*
 * cdk_main.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __CDK_MAIN_H__
#define __CDK_MAIN_H__

#include <iostream>
#include <vector>
#include <cdk_test.h>
#include <sigc++/sigc++.h>

#include <globals.h>

C_LINKAGE_BEGIN
#include <sigact.h>
#include <inot.h>

#include <ubus/libubus.h>
#include <libubox/blobmsg_json.h>
C_LINKAGE_END

#define MENU_ITEMS 4

namespace CdkSnetaid
{

using namespace std;

class CdkMain
{
public:
   CdkMain(string, string);
   virtual ~CdkMain();
  
protected:
   CDKSCREEN *cdkscreen;
   CDKMENU *menu;
   CDKLABEL *cdklabel;
   
   sigc::connection *update_connector;
   
   int x, y, status;
   bool connected_ok;
   int keystroke, submenusize[MENU_ITEMS], menuloc[MENU_ITEMS];
   string wired_device, wireless_device;
   string essid, passwd, filename;

   int getcCDKObject(CDKOBJS*);
   int wrapped(int, int);
   void drawItem(CDKMENU*, int, int);
   void selectItem(CDKMENU*, int, int);   
   void acrossSubmenus(CDKMENU*, int);
   void withinSubmenu(CDKMENU*, int); 
   int activateCDKMenu(CDKMENU*);
   int getchCDKObject(CDKOBJS*, boolean*);
   void display();
   void get_status();  
   void cleanUpMenu();
   void show_details();
   void show_device(string);
   void show_info();
   int scrolled_list();
   int show_saved_wifis();
   void passwd_entry();
   void filename_entry();
   
   // Callbacks
   void on_callback_ui_display();
   void on_callback_ui_update();
   void on_callback_ui_delete();
};

} // namespace CdkSnetaid

#endif // __CDK_MAIN_H__

/*
 * main.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
   

#include <iostream>
#include <memory>
#include <vector>

#include <stdbool.h>
#include <signal.h>
#include <unistd.h>
#include <cassert>
#include <assert.h>
#include <libgen.h>   // basename()
#include <unistd.h>
#include <sys/wait.h>
#include <cstring>
#include <algorithm>
#include <proc/readproc.h>
#include <sys/sysmacros.h>

#include <globals.h>
#include "def.h"
#include "cdk_main.h"
#include "functions_ui.h"

#define MAX_SIZE 1024

#define strdup_or_null(str)  (str) != NULL ? strdup(str) : NULL

functions_Ui * f_ui;

C_LINKAGE_BEGIN
#include <simple-netaid/sbuf.h>
#include <simple-netaid/helpers.h>
#include <simple-netaid/ethlink.h>
#include <pstat/libpstat.h>
#include <inot.h>
#include <proc.h>
#include <sigact.h>

void get_interfaces(struct sbuf*, struct sbuf*);

functions_Ui* new_functions_ui() {
   return new functions_Ui();
}
  
void ui_update() {
   return f_ui->do_update();
}
  
void ui_display() {
   return f_ui->do_display();
}
  
void ui_delete() {
   return f_ui->do_delete();
}
C_LINKAGE_END

const char *watchdir = "/tmp/snetaid1000";
const char *lockfile = "/tmp/snetaid1000/snetaid.lock";
const char *nl_monitor_path = "/tmp/snetaid1000/rt_netlink.log";

uint32_t host_id; /* ering host id */ 
const char *ubus_socket = NULL; /* use default UNIX sock path: /var/run/ubus.sock */ 
struct ubus_context *ctx;
struct blob_buf bb_t;

static void nl_monitor_subscribe_cb(struct ubus_context *ctx, struct ubus_object *obj)
{
	fprintf(stderr, "Subscribers active: %d\n", obj->has_subscribers);
}

/* ubus object assignment */
struct ubus_object cli_obj =
{
   .name = "netaid-client",
   .subscribe_cb = nl_monitor_subscribe_cb,
#if 0
   .type = &cli_obj_type,
   .methods = cli_methods,
   .n_methods = ARRAY_SIZE(cli_methods),
   .path= /* useless */
#endif
};

const char *progname="simple-netaid-cdk";
int p = 0;

// m_tty: client's tty, but not parent's tty - the parent is the container, 
// the client is one of the asynchronous process started in the vte terminal
int m_tty = -1;

char m_essid[256] = {0};
char m_passwd[256] = {0};
char m_filename[256] = {0};

using namespace std;

void loopMain(int*, string, string);
char *str_slice(char str[], int slice_from, int slice_to);

int ubus_lock(const char*);
int ubus_unlock(const char*);
int ubus_monitor(const char*);
int ubus_interface_up(const char*);
int ubus_interface_down(const char*);
int ubus_ipaddr_flush(const char*);
int ubus_ifup(const char*, int);
int ubus_ifdown(const char*, int);
int ubus_wpa_passphrase(const char*, const char*, const char*);
int ubus_wpa_supplicant(const char*, const char*, int);
int ubus_uninstall(const char*);
void ubus_disconnect(const char*, int);
void ubus_wired_connection(const char*, int);
void ubus_wireless_connection(const char*, const char*, const char*, const char*, int);

using std::vector;

#define MAX_SIZE 1024

/**
 * \brief Callback function for ubus_invoke to process result from the host
 * Here we just print out the message.
 */
static
void result_handler(struct ubus_request *req, int type, struct blob_attr *msg_t)
{
   char *strmsg = NULL;
   if(!msg_t) return;
   strmsg=blobmsg_format_json_indent(msg_t,true, 0); /* 0 type of format */
   //printf("Response from the host: %s\n", strmsg);
   free(strmsg);
}

static
void wait_on_ifupdown(int time)
{
   FILE *fp = NULL;
   char ls[128] = {0};
   
   fp = popen("ls /var/run/network | grep .pid", "r");
    if(!fp) {
       printf("Sorry, there was an error opening the pipe\n");
       exit(EXIT_FAILURE);
    }
   
    usleep(time);
    if(fgets(ls, 128, fp)!=NULL) {
	   char path[128] = {0};
	   ls[strcspn(ls, "\n")] = 0;
	   snprintf(path, sizeof(path), "/var/run/network/");
	   strcat(path, ls);
       while(!access(path, F_OK )) sleep(1);
    }
    pclose(fp);
}   

// send SIGUSR1 with integer value = getpid() to the container
static
void send_signal_with_value(int signum)
{
   union sigval sv;
   sv.sival_int = getpid();   
      
   if(sigqueue(getppid(), signum, sv) != 0) {
      perror("SIGSENT-ERROR:");
   }
}

// TODO: define arguments in the command line
int main(int argc, char *argv[])
{
             
   int rc = 0;
   struct sbuf wired, wireless; 
   
   signal(SIGUSR1, SIG_IGN); 
   
   PROCTAB *proc = openproc(PROC_FILLSTAT);

   while(proc_t *proc_info = readproc(proc, nullptr)) {

      if((proc_info->ppid == getppid()) && !strcmp(proc_info->cmd, "tail")) {
            
         m_tty = (int)minor(proc_info->tty);
         send_signal_with_value(SIGUSR1);
      }

      freeproc(proc_info);
   }
   
   closeproc(proc);
 
   // internationalization
   /*
   {
       int j;
       const char *var[3] = {"LC_MESSAGES", "LC_ALL", "LANG"};
       const char *value, *mylocale __attribute__((unused));
       for(j=0; j<3; j++)
         {
            value=getenv(var[j]);
            // Unset
            setenv(var[j], "", 1);
            if(value) break;
         }
       setenv("LC_CTYPE", "", 1);
       mylocale = setlocale(LC_MESSAGES, value);    
       bindtextdomain(progname, "/usr/local/share/locale");
       textdomain(progname);
   }
   */
	
   sbuf_init(&wired);
   sbuf_init(&wireless);
   get_interfaces(&wired, &wireless);
   
   string wired_device = string(wired.buf);
   string wireless_device = string(wireless.buf);
   
   sigaction_init();
  
   //if(m_tty<0) 
      run_async_inotinit(watchdir, wired.buf, wireless.buf);
   
   sbuf_free(&wired);
   sbuf_free(&wireless);
   
   f_ui = new functions_Ui();   
   CdkSnetaid::CdkMain *cdk_main = new CdkSnetaid::CdkMain(wired_device, wireless_device);

   uloop_init();
	
   ctx=ubus_connect(ubus_socket);
   if(ctx==NULL) {
      printf("Fail to connect to ubusd!\n");
	  return 1;
   }
	
   ubus_add_uloop(ctx);
	
   rc=ubus_add_object(ctx, &cli_obj);
   if(rc!=0) {
      printf("Fail to register an object to ubus.\n");
      goto Ubus_fail;
 
   } else {
      printf("Add '%s' to ubus @%u successfully.\n", cli_obj.name, cli_obj.id);
   }
   
   if( ubus_lookup_id(ctx, "ering.netaid", &host_id) ) {
	  printf("ering.netaid is NOT found in ubus!\n");
      goto Ubus_fail;
   }
   printf("ering.netaid is found in ubus @%u.\n", host_id);
   
   for ( ;; ) {
      loopMain(&p, wired_device, wireless_device);
      if(p == QUIT) break;
   }
   delete f_ui;
   delete cdk_main;
   
   endCDK();
 
   uloop_done();
	
Ubus_fail:
   ubus_free(ctx);
   cout << "Exited gratefully.\n" << endl;
  
   ExitProgram(EXIT_SUCCESS);
}

void loopMain(int *p, string wired_device, string wireless_device)
{
   sigset_t mask;
  
   sigemptyset(&mask);
   sigaddset(&mask, SIGUSR1);
   sigprocmask(SIG_BLOCK, &mask, NULL);

   if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
      fprintf(stderr, "Cannot block SIGUSR1: %s.\n", strerror(errno));
      exit(EXIT_FAILURE);
   }

   switch (*p) {
	  case QUIT:
	     return;  /* end the loop */
      case WIRED_UP:
         ubus_interface_up(wired_device.c_str());
         sleep(1);
         break;
      case WIRED_DOWN:
         ubus_interface_down(wired_device.c_str());
         sleep(1);
         break;
      case WIRED_DISCONNECT:
         {
            ubus_disconnect(wired_device.c_str(), m_tty);
            wait_on_ifupdown(10);
            int count = 0;
            while(!ethlink(wired_device.c_str())) {
               sleep(1);
               if(count > 10) break;
               count++;
            }
         }         
         break;
      case WIRED_CONNECTION:
         ubus_wired_connection(wired_device.c_str(), m_tty);
         wait_on_ifupdown(10);
         break;
      case WIRELESS_UP:
         ubus_interface_up(wireless_device.c_str());
         sleep(1);
         break;
      case WIRELESS_DOWN:
         ubus_interface_down(wireless_device.c_str());
         sleep(1);
         break;
      case WIRELESS_DISCONNECT:
         ubus_disconnect(wireless_device.c_str(), m_tty);
         wait_on_ifupdown(10);
         break;
      case WIRELESS_CONNECTION:
         ubus_wireless_connection(wireless_device.c_str(), m_essid, m_passwd, (char*)0, m_tty);
         wait_on_ifupdown(10);
         sleep(1);
         break;
      case INSTALL_AND_CONNECT:
         {
            string filename = "/etc/network/wifi/" + string(m_filename);
            ubus_wireless_connection(wireless_device.c_str(), m_essid, m_passwd, filename.c_str(), m_tty);
            wait_on_ifupdown(10);
         }
         break; 
      case CONNECT_TO_SAVED:
         ubus_wireless_connection(wireless_device.c_str(), m_essid, m_passwd, m_filename, m_tty);
         wait_on_ifupdown(2);
         sleep(1);
         break;         
      case UNINSTALL:
         ubus_uninstall(m_filename);
         break;
      case DEFAULT:                  
      default:
         break;
   }

   if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1) {
      fprintf(stderr, "Cannot unblock SIGUSR1: %s.\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
 
   ui_display();
}

/**
 * Extracts a selection of string and return a new string or NULL.
 * It supports both negative and positive indexes.
 */
char *
str_slice(char str[], int slice_from, int slice_to)
{
    // if a string is empty, returns nothing
    if (str[0] == '\0')
        return NULL;

    char *buffer;
    size_t str_len, buffer_len;

    // for negative indexes "slice_from" must be less "slice_to"
    if (slice_to < 0 && slice_from < slice_to) {
        str_len = strlen(str);

        // if "slice_to" goes beyond permissible limits
        if (abs(slice_to) > str_len - 1)
            return NULL;

        // if "slice_from" goes beyond permissible limits
        if (abs(slice_from) > str_len)
            slice_from = (-1) * str_len;

        buffer_len = slice_to - slice_from;
        str += (str_len + slice_from);

    // for positive indexes "slice_from" must be more "slice_to"
    } else if (slice_from >= 0 && slice_to > slice_from) {
        str_len = strlen(str);

        // if "slice_from" goes beyond permissible limits
        if (slice_from > (int)str_len - 1)
            return NULL;

        buffer_len = slice_to - slice_from;
        str += slice_from;

    // otherwise, returns NULL
    } else
        return NULL;

    buffer = (char*)calloc(buffer_len, sizeof(char));
    strncpy(buffer, str, buffer_len);
    return buffer;
}

/*========================= Ubus ============================================*/

int ubus_lock(const char *lockfile)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "filename", lockfile);
   return ubus_invoke(ctx, host_id, "lock", bb_t.head, result_handler, 0, 3000);
}

int ubus_unlock(const char *lockfile)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "filename", lockfile);
   return ubus_invoke(ctx, host_id, "unlock", bb_t.head, result_handler, 0, 3000);
}

int ubus_monitor(const char *nl_monitor_path)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "filename", nl_monitor_path);
   return ubus_invoke(ctx, host_id, "nl_monitor", bb_t.head, result_handler, 0, 3000);
}

int ubus_interface_down(const char *ifname)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "ifname", ifname);
   return ubus_invoke(ctx, host_id, "interface_down", bb_t.head, result_handler, 0, 3000);
}

int ubus_interface_up(const char *ifname)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "ifname", ifname);
   return ubus_invoke(ctx, host_id, "interface_up", bb_t.head, result_handler, 0, 3000);
}

int ubus_ipaddr_flush(const char *ifname)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "ifname", ifname);
   return ubus_invoke(ctx, host_id, "ipaddr_flush", bb_t.head, result_handler, 0, 5000);
}

int ubus_ifdown(const char *ifname, int tty)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "ifname", ifname);
   blobmsg_add_u32(&bb_t, "tty", tty);
   return ubus_invoke(ctx, host_id, "ifdown", bb_t.head, result_handler, 0, 5000);
}

int ubus_ifup(const char *ifname, int tty)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "ifname", ifname);
   blobmsg_add_u32(&bb_t, "tty", tty);
   return ubus_invoke(ctx, host_id, "ifup", bb_t.head, result_handler, 0, 5000);
}

int ubus_wpa_passphrase(const char *essid, const char *passwd, const char *filename)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t, "id", cli_obj.id);
   blobmsg_add_string(&bb_t, "essid", essid);
   blobmsg_add_string(&bb_t, "passwd", passwd);
   blobmsg_add_string(&bb_t, "filename", filename);
   return ubus_invoke(ctx, host_id, "wpa_passphrase", bb_t.head, result_handler, 0, 5000);
}

int ubus_wpa_supplicant(const char *ifname, const char *filename, int tty)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "ifname", ifname);
   blobmsg_add_string(&bb_t, "filename", filename);
   blobmsg_add_u32(&bb_t, "tty", tty);
   return ubus_invoke(ctx, host_id, "wpa_supplicant", bb_t.head, result_handler, 0, 5000);
}

int ubus_uninstall(const char *filename)
{
   blob_buf_init(&bb_t,0);
   blobmsg_add_u32(&bb_t,"id", cli_obj.id);
   blobmsg_add_string(&bb_t, "filename", filename);
   return ubus_invoke(ctx, host_id, "uninstall", bb_t.head, result_handler, 0, 3000);
}

void ubus_disconnect(const char *ifname, int tty)
{
   int rc __attribute__((unused)) = 0;
   rc = ubus_ifdown(ifname, tty);
   rc = ubus_interface_down(ifname);
   rc = ubus_interface_up(ifname);
}

void ubus_wired_connection(const char *ifname, int tty)
{
   int rc __attribute__((unused)) = 0;
   rc = ubus_ipaddr_flush(ifname);
   rc = ubus_interface_down(ifname);
   rc = ubus_ifdown(ifname, tty);
   rc = ubus_interface_up(ifname);
   sleep(1);
   rc = ubus_ifup(ifname, tty);
}

// filename != NULL -> we're going to install it, but not if it already exists
// filename == NULL -> wpa_passphrase creates a file in /tmp, being it removed afterwards 
void ubus_wireless_connection(const char *ifname, const char *essid, const char *password, const char *filename, int tty)
{
   int rc __attribute__((unused)) = 0;
   struct sbuf s;
   
   sbuf_init(&s);
   if(filename) sbuf_addstr(&s, filename);
   else {
      sbuf_addstr(&s, "/tmp/simple-netaid_");
      sbuf_random(&s, 5);
   }
   
   rc = ubus_ipaddr_flush(ifname);
   rc = ubus_interface_down(ifname);
   rc = ubus_ifdown(ifname, tty);
   rc = ubus_interface_up(ifname);
   if(access(filename, F_OK) != 0) {
      rc = ubus_wpa_passphrase(essid, password, s.buf);
   }
   rc = ubus_wpa_supplicant(ifname, s.buf, tty);
   sleep(1);
   rc = ubus_ifup(ifname, tty);
   
   sbuf_free(&s);  
}

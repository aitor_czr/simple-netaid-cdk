# simple-netaid-cdk

Text user interface for simple-netaid developed with the Curses Development Kit (CDK), 
based on the ncurses library. Simple-netaid is a networking management tool with minimal 
dependencies and easy to use, without the need of an specific Desktop Environment.

<p align="center">
  <img src="screenshots/screenshot.png">
</p>


## Installation

Use the [APT](https://wiki.debian.org/Apt) package manager to install simple-netaid-cdk.

```bash
apt-get install simple-netaid-cdk
```

### Building your own packages

You can build your own debian packages in a very easy way using `git-buildpackage` (a wrapper for `dpkg-buildpackage`). 
All you need to do is:

```sh
$ git clone https://git.devuan.org/aitor_czr/simple-netaid-cdk.git
$ cd simple-netaid-cdk
```

If you type `git status` you can see all:

```sh
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working directory clean
```

To list all the local and remote branches type:

```sh
$ git branch -a
```

Let's checkout on the following branches (in this order):

```sh
$ git checkout -b pristine-tar origin/pristine-tar
$ git checkout -b gbp-release_0.1 origin/gbp-release_0.1
```

The prefix `gbp` reflects those branches containing the debian folder. 
Now we can build the packages:

```sh
$ gbp buildpackage
```

Once the packages have been generated, go to the build directory and install them:

```sh
$ cd ../build-area
$ sudo dpkg -i *.deb
```


## Building from source

If you are using a different package management system, then you'll need to build the project from source.

### Build dependencies

The current version of snetaid has been tested under Devuan Beowulf, requiring:
- `gcc (4:8.3.0-1)`
- `cmake (3.13.4-1)`
- `libcdk5-dev (5.0.20180306-3)`
- `libnetaid-dev (0.1-1)`

Practical dependencies at run time:
- `wpasupplicant`
- `ifupdown` 

libnetaid is available [here](https://git.devuan.org/aitor_czr/libnetaid).


### Procedure

Download the tarball from the git repository of devuan [simple-netaid-cdk](https://git.devuan.org/aitor_czr/simple-netaid-cdk/archive/master.tar.gz), 
extract it with `tar xvf master.tar.gz` and go into the extracted folder. There, run the 
following commands: 

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
$ sudo make install
```
> **Note**: By default, CMake does not provide the "make uninstall" target; so, in general, you cannot do this. 
However, snetaid adds an uninstall target to the CMAKE generated Makefile, in such a way that you can run:

```sh
$ sudo make uninstall
```


## Usage

The usage of simple-netaid-cdk is very intuitive, since it consists of a very simple and user-friendly interface. 
You need root permissions to run the application. All the saved active wifis are installed in `/etc/network/wifi` 
by `wpa_passphrase`.

<p align="center">
  <img src="screenshots/screenshot_2.png">
</p>

If you want to automatically connect on boot, then you need to install the 
[snetaid](https://git.devuan.org/aitor_czr/snetaid.git) service.


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what 
you would like to change.

Please make sure to update tests as appropriate.


## License

simple-netaid-cdk is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, either version 3 
of the License, or (at your option) any later version.

simple-netaid-cdk is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. 
If not, see <http://www.gnu.org/licenses/>.

See the COPYING file.


DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH 2>/dev/null || true)

CDK_DIR   := cdk
LIBS_DIR := libs

VPATH := $(CDK_DIR):$(LIBS_DIR)

DESTDIR ?=
PREFIX ?= /usr/local
BINDIR ?= $(DESTDIR)$(PREFIX)/bin

CDK_DEPS  := $(shell $(MAKE) -s -r -C $(CDK_DIR) print_deps)
LIBS_DEPS := $(shell $(MAKE) -s -r -C $(LIBS_DIR) print_deps)

INSTALLED_FILES = $(BINDIR)/simple-netaid-cdk

.SUFFIXES:

all: $(CDK_DIR)/simple-netaid-cdk

$(CDK_DIR)/simple-netaid-cdk: $(CDK_DEPS)
	@$(MAKE) -r -C $(CDK_DIR) simple-netaid-cdk

$(LIBS_DIR)/libnetaidcdk.a: $(LIBS_DEPS)
	@$(MAKE) -r -C $(LIBS_DIR) libnetaidcdk.a

install: $(INSTALLED_FILES)

$(BINDIR)/simple-netaid-cdk: $(CDK_DIR)/simple-netaid-cdk
	cp $< $@

clean:
	$(MAKE) -r -C $(LIBS_DIR) clean
	$(MAKE) -r -C $(CDK_DIR) clean

cleanall:
	$(MAKE) -r -C $(LIBS_DIR) cleanall
	$(MAKE) -r -C $(CDK_DIR) cleanall
	rm -vf $(wildcard *~)

uninstall:
	rm -vf $(INSTALLED_FILES)

.PHONY: clean cleanall uninstall
.SILENT: clean cleanall uninstall

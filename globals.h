  
#ifndef __GLOBALS_H__
#define __GLOBALS_H__

#ifndef _BSD_SOURCE
#define _DEFAULT_SOURCE 1
#define _BSD_SOURCE
#endif

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifdef __cplusplus
#define C_LINKAGE_BEGIN extern "C" {
#define C_LINKAGE_END }
#else
#define C_LINKAGE_BEGIN 
#define C_LINKAGE_END
#endif

#include <cdk_test.h>
#include <stdbool.h>

C_LINKAGE_BEGIN
#include <simple-netaid/sbuf.h>

#include <ubus/libubus.h>
#include <libubox/blobmsg_json.h>
C_LINKAGE_END

extern uint32_t host_id; /* ering host id */ 
extern const char *ubus_socket; /* use default UNIX sock path: /var/run/ubus.sock */
extern struct ubus_context *ctx;
extern struct blob_buf bb_t; 

/* ubus object assignment */
extern struct ubus_object cli_obj;

struct functions_Ui;
typedef struct functions_Ui functions_Ui;
extern functions_Ui * f_ui;

/*====================== Functions the UI shall provide =====================*/
C_LINKAGE_BEGIN
void ui_update();
void ui_display();
void ui_delete();
C_LINKAGE_END

/*-------------------------- Global constant --------------------------------*/
extern char arg0[256];
extern const char *libpath;
extern int p, m_tty;
extern const char *lockfile;
extern pid_t ppid;

extern void killchild();

/*--------------------- Internationalization --------------------------------*/
#include <locale.h>
#include <libintl.h>
#define _(StRiNg)  dgettext("simple-netaid-cdk", StRiNg)

#endif  // __GLOBALS_H__

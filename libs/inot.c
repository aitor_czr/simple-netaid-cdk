/*
  This subprogram initializes the inotify filedescriptor and watches used
  to monitor the insertion and ejection of hot-plug disk partitions (USB keys
  or MMC devices). The main program is supposed to have performed a chdir to
  /dev before calling this subprogram.

  Two watches are created, one for file creation and deletion in /dev and one
  for file creation in /dev/disk/by-label, because the hotplugger (eg udev)
  creates in this directory symbolic links to the device files in /dev, named
  after the filesystem labels.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <inttypes.h>
#include <wait.h>
#include <sys/socket.h>
#define DATA1 ""
#define DATA2 "Sending DATA2"

#include <simple-netaid/iproute.h>
#include <simple-netaid/interfaces.h>

#include <cdk_test.h>

#include "inot.h"
#include <globals.h>

#define MAX_SIZE 1024

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define EVENT_BUF_LEN  ( 1024 * ( EVENT_SIZE + 16 ) )


/*------------------ initialize file event notification -------------------*/
void inotinit(const char *watchdir, const char *wired_device, const char *wireless_device, pid_t ppid)
{
   int inotfd;
   int wfd;  /* the watch descriptor */
   int status_old;
  
   inotfd = inotify_init1(IN_CLOEXEC);
   if(inotfd < 0) {
      fprintf(stderr, "cannot initialize inotify: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }

   wfd = inotify_add_watch(inotfd, watchdir, IN_CREATE | IN_DELETE | IN_MODIFY);
   if(wfd == -1) {
      fprintf(stderr, "cannot add inotify watch: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
      
   status_old = get_current_state(wired_device, wireless_device);

   while(1) {
      int i = 0;
      int length, status_new;
      char buffer[EVENT_BUF_LEN]={0};
      bool changed_ok = false;

      // read to determine the event change happens on the monitoring list. 
      // Actually this read blocks until the change event occurs
      length = read(inotfd, buffer, EVENT_BUF_LEN);  
      if(length < 0) {
         fprintf(stderr, "cannot add inotify watch: %s\n", strerror(errno));
         continue;
      }

      // Actually read return the list of change events happens. 
      // Here, read the change event one by one and process it accordingly.
      while(i < length) {     
         struct inotify_event *event = (struct inotify_event *) &buffer[i];     
         if(event->len && (event->mask & IN_MODIFY))
            changed_ok = true;     
         else if(event->len && (event->mask & IN_DELETE)) {
		    if(access(lockfile, F_OK) != 0)
               kill(ppid, SIGTERM);
         } 
         i += EVENT_SIZE + event->len;
      }
      
      if(!changed_ok)
         continue;

      status_new = get_current_state(wired_device, wireless_device);
      if(status_new != status_old)
		 kill(ppid, SIGUSR1);

      status_old = status_new;
   }
}

int get_current_state(const char *wired_device, const char *wireless_device)
{   
   int a=1; /* disconnected */
   int b=get_interface_status(wired_device);
   int c=get_interface_status(wireless_device);
   
   const char *ifname = iproute();
   
   if(strcmp(ifname, "")) {
      if(!strcmp(ifname, wired_device)) a=2;
      else if(!strcmp(ifname, wireless_device)) a=3;
   }
   
   return (a*100 + b*10 +c);      
}

void run_async_inotinit(const char *watchdir, const char *wired_device, const char *wireless_device)
{
   pid_t pid = 0;
   pid_t sid = 0;
   int sockets[2];
   //char buf[1024];
   
   if (socketpair(AF_UNIX, SOCK_STREAM, 0, sockets) < 0) {
	  perror("opening stream socket pair");
	  exit(1);
   }
   
   pid = fork();
      
   if( pid == 0 ) {  /* child */
	   
	  pid_t ppid = getppid();
           
      // detach from parent 
      sid = setsid();
      if( sid < 0 ) {
         fprintf(stderr, "setsid() failed: %s\n", strerror(errno) );
         exit(1);
      }
           
      close(sockets[1]);
            
      //if (write(sockets[0], DATA2, sizeof(DATA2)) < 0)
        // perror("writing stream message");
      
      close(sockets[0]); 
      return inotinit(watchdir, wired_device, wireless_device, ppid);

   }
   else if( pid > 0 ) {
	 
      close(sockets[0]);
      //if (read(sockets[1], buf, 1024) < 0)
        // perror("reading stream message");
      
      close(sockets[1]);
   }
   else {

      fprintf(stderr, "fork() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
}


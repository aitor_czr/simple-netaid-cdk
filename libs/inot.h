 /*
  * sigact.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  
  
#ifndef __INOT_H__
#define __INOT_H__

#include <stdio.h>
#include <stdlib.h>

void inotinit(const char*, const char*, const char*, pid_t);
bool is_locked(const char*);
int get_current_state(const char*, const char*);
void run_async_inotinit(const char*, const char*, const char*);

#endif  //  __INOT_H__

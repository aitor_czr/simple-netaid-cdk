 /*
  * sigact.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>

#include <cdk_test.h>

#include "sigact.h"
#include "proc.h"
#include <globals.h>

#define MAX_SIZE 1024

static
void hdl(int signum, siginfo_t *info, void *extra)
{
   switch(signum)
   {
      case SIGALRM:
         break;
      case SIGCHLD:
         break;
      case SIGHUP:
         break;
      case SIGUSR1:
         //signal(SIGUSR1, SIG_IGN);
         ui_update();
         sigaction_init();
         break;
      case SIGUSR2:
         break;
      case SIGTERM:
      case SIGINT:
      case SIGQUIT:
         /* Make sure to cleanup if user sends a keyboard interrupt */
         ui_delete();
         break;
   }
}

void sigaction_init()
{
   struct sigaction sa;
   memset (&sa, 0, sizeof(sa));
      
   /*---------------- initialize signal handling ------------ */
   sa.sa_sigaction = &hdl;
   sa.sa_flags = SA_RESETHAND | SA_RESTART | SA_NODEFER; // | SA_SIGINFO
   sigemptyset(&sa.sa_mask);
   sigaction(SIGALRM, &sa, 0);
   sigaction(SIGCHLD, &sa, 0);
   sigaction(SIGHUP,  &sa, 0);
   sigaction(SIGUSR1, &sa, 0);
   sigaction(SIGUSR2, &sa, 0);
   sigaction(SIGTERM, &sa, 0);
   sigaction(SIGINT,  &sa, 0);
   sigaction(SIGQUIT, &sa, 0);
}

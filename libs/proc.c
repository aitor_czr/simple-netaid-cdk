 /*
  * proc.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <proc/readproc.h>

#include <simple-netaid/sbuf.h>
#include <pstat/libpstat.h>

#include "proc.h"
#include <globals.h>

#define MAX_SIZE 1024

// Usage: strncpy_t(str1, sizeof(str1), str2, strlen(str2));
// Copy string "in" with at most "insz" chars to buffer "out", which
// is "outsz" bytes long. The output is always 0-terminated. Unlike
// strncpy(), strncpy_t() does not zero fill remaining space in the
// output buffer:
char* strncpy_t(char* out, size_t outsz, const char* in, size_t insz)
{
   assert(outsz > 0);
   while(--outsz > 0 && insz > 0 && *in) { *out++ = *in++; insz--; }
   *out = 0;
   return out;
}

/**
 * \brief Extracts a selection of string and return a new string or NULL.
 *   It supports both negative and positive indexes.
 */
char* str_slice(char str[], int slice_from, int slice_to)
{
    // if a string is empty, returns nothing
    if (str[0] == '\0')
        return NULL;

    char *buffer;
    size_t str_len, buffer_len;

    // for negative indexes "slice_from" must be less "slice_to"
    if (slice_to < 0 && slice_from < slice_to) {
        str_len = strlen(str);

        // if "slice_to" goes beyond permissible limits
        if (abs(slice_to) > str_len - 1)
            return NULL;

        // if "slice_from" goes beyond permissible limits
        if (abs(slice_from) > str_len)
            slice_from = (-1) * str_len;

        buffer_len = slice_to - slice_from;
        str += (str_len + slice_from);

    // for positive indexes "slice_from" must be more "slice_to"
    } else if (slice_from >= 0 && slice_to > slice_from) {
        str_len = strlen(str);

        // if "slice_from" goes beyond permissible limits
        if (slice_from > str_len - 1)
            return NULL;

        buffer_len = slice_to - slice_from;
        str += slice_from;

    // otherwise, returns NULL
    } else
        return NULL;

    buffer = calloc(buffer_len, sizeof(char));
    strncpy(buffer, str, buffer_len);
    return buffer;
}


int kill_child(int signal)
{   
   int count = 0;   
   PROCTAB *proc = openproc(PROC_FILLSTAT);

   while (1) {
	  proc_t *proc_info = NULL;
	  proc_info = readproc(proc, NULL);	
      if(proc_info == NULL) break;
      if(!strcmp(str_slice(proc_info->cmd, 0, 13), "simple-netaid") && proc_info->ppid == getpid()) {
         bool res = false;
         res = filter_proc(proc_info->tgid);
         if(res) {
            count++;
            int child_pid = proc_info->tgid;
            if(child_pid != (uint32_t)getpid())
               kill(child_pid, signal);
         }
      }

      freeproc(proc_info);
   }
   
   closeproc(proc);
       
   return count;
}

bool filter_proc(int child_pid)
{
   char haystack[MAX_SIZE] = {0};
   char cmd[64]={0};
   FILE *pf = NULL;
   bool result_ok = false;
   char *parent = NULL;
   char *child = NULL;
   int rc;
   
   // Do they match?
   // parent -> path to the binary of the running process gotten from getpid() via pstat
   // child -> path to the binary of the process to be inspected gotten from child_pid via pstat
   // NOTE: scripts like '/etc/init.d/snetaid' will return "/bin/dash", 
   //       "/bin/bash", and so on, depending on the shebang.
   rc = pstat_get_binary_path_from_integer(&parent, 128, (uint32_t)getpid());     
   if(rc!=0) {
	  free(parent);
	  exit(EXIT_FAILURE);
   }
   rc = pstat_get_binary_path_from_integer(&child, 128, child_pid);     
   if(rc!=0) {
	  free(child);
	  exit(EXIT_FAILURE);
   }
   if(strcmp(parent, child)) {
      free(parent);
      free(child);
      return false;
   }
   free(parent);
   
   // They match, however we won't blithely kill the process
   // Make sure it depends on libnetaid, before killing it.
   // This step might be redundant, as we already have compared 
   // both paths. But go ahead anyway...
   strncpy_t(cmd, sizeof(cmd), "ldd ", 4);
   strcat(cmd, child);
   strcat(cmd, " | grep libnetaid.so");
   pf = popen(cmd, "r");
   if(!pf) { 
      free(child);
      fprintf(stderr, "popen(): %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }   
   if(fgets(haystack, MAX_SIZE, pf)) {
      // The following verification is recommended because ldd might return a 
      // warning. In such case, we would get a non empty output.
      const char *needle = "libnetaid.so";
      if(strstr(haystack, needle)) result_ok = true;
   }
   
   free(child);
   pclose(pf);
      
   return result_ok;
}
